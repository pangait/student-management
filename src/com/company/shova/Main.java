package com.company.shova;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    private static List<Student> DB = new ArrayList<>();

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int input = 0;
        do {
            System.out.println("What do you want to do? ");
            System.out.println("1. Add");
            System.out.println("2. Delete");
            System.out.println("3. Get By Id");
            System.out.println("4. List");
            System.out.println("5. Update");
            System.out.println("6. Exit");
            input = in.nextInt();

            if (input == 1){
                addStudent();
            }else if (input == 4){
                listStudents();
            }else if (input == 2){
                removeStudent();
            }else if (input == 3){
                showStudentById();
            }else if (input == 5){
                updateStudent();
            }

        }
        while(input != 6);

    }

    private static void updateStudent() {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter the id to update ");
        int id = in.nextInt();
        for (int i = 0; i < DB.size(); i++) {
            Student student = DB.get(i);
            if (id == student.getStudentId()){
                System.out.println("Enter the name ");
                String name = in.next();
                System.out.println("Enter the age ");
                int age = in.nextInt();
                student.setName(name);
                student.setAge(age);
                System.out.println("Updated student: " + student);
                break;
            }

        }
    }

    private static void showStudentById() {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter student id to view ");
        int id = in.nextInt();
        for (int i = 0; i < DB.size(); i++) {
            Student s = DB.get(i);
            if (id == s.getStudentId()){
                System.out.println("Student details: " + s);
                break;
            }

        }
    }

    private static void removeStudent() {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter id to remove the student");
        int id = in.nextInt();
        for (int i = DB.size()-1; i >= 0; i--) {
            Student s = DB.get(i);
            if(id == s.getStudentId()){
                DB.remove(i);
                System.out.println("Removed: " + s);
                break;
            }

        }
    }

    private static void listStudents() {
        System.out.println("Student list:");
        System.out.println(DB);
    }

    private static void addStudent() {

        Scanner in = new Scanner(System.in);
        System.out.println("Enter student detail");
        System.out.println("Enter Student id:");
        int id = in.nextInt();
        System.out.println("Enter student name:");
        String name = in.next();
        System.out.println("Enter student age:");
        int age = in.nextInt();
        Student student = new Student(id, name, age);

        DB.add(student);
    }
}
